package sample.Model;

import sample.BCrypt.BCrypt;
import sample.ObjectProperty.ItemProperty;
import sample.ObjectProperty.orderProperty;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.*;
import java.util.*;

public class Datasource {
    public static final String DB_NAME = "warehouse.db";
    public static final String CONNECTION_STRING = "jdbc:sqlite:" + DB_NAME;

    public static final String TABLE_USERS = "users";
    public static final String COLUMN_USERS_FIRSTNAME = "firstname";
    public static final String COLUMN_USERS_LASTNAME = "lastname";
    public static final String COLUMN_USERS_USERNAME = "username";
    public static final String COLUMN_USERS_PASSWORD = "password";
    public static final String COLUMN_USERS_ISADMIN = "isadmin";
    public static final String COLUMN_USERS_ID = "_id";

    public static final String TABLE_ITEMS = "items";
    public static final String COLUMN_ITEMS_NAME = "name";
    public static final String COLUMN_ITEMS_PRICE = "price";
    public static final String COLUMN_ITEMS_STOCK = "stock";
    public static final String COLUMN_ITEMS_ID = "_id";

    public static final String TABLE_ORDERS = "orders";
    public static final String COLUMN_ORDERS_USERID = "userid";
    public static final String COLUMN_ORDERS_ID = "_id";
    //public static final String COLUMN_ORDERS_BASKETID = "basketid";
    public static final String COLUMN_ORDERS_PRICE = "price";
    public static final String COLUMN_ORDERS_AMOUNT = "amount";

    public static final String TABLE_BASKET = "basket";
    public static final String COLUMN_BASKET_ITEMID = "itemid";
    public static final String COLUMN_BASKET_AMOUNT = "amount";
    //public static final String COLUMN_BASKET_USERID = "userid";
    public static final String COLUMN_BASKET_ID = "_id";


    public static final String CREATE_USERS_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_USERS + "(" + COLUMN_USERS_ID + " INTEGER PRIMARY KEY, " +
            COLUMN_USERS_USERNAME + " TEXT NOT NULL, " + COLUMN_USERS_PASSWORD + " TEXT NOT NULL, " + COLUMN_USERS_FIRSTNAME + " TEXT NOT NULL, " +
            COLUMN_USERS_LASTNAME + " TEXT NOT NULL, " + COLUMN_USERS_ISADMIN + " INTEGER)";
    private PreparedStatement createUsersTable;

    public static final String CREATE_ITEMS_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_ITEMS + "(" + COLUMN_ITEMS_ID + " INTEGER PRIMARY KEY, " +
            COLUMN_ITEMS_NAME + " TEXT NOT NULL, " + COLUMN_ITEMS_PRICE + " REAL, " + COLUMN_ITEMS_STOCK + " INTEGER)";
    private PreparedStatement createItemsTable;

    public static final String CREATE_ORDERS_TABLE =  "CREATE TABLE IF NOT EXISTS " + TABLE_ORDERS + "(" + COLUMN_ORDERS_ID + " INTEGER PRIMARY KEY, " +
            COLUMN_ORDERS_USERID  + " INTEGER, " +  COLUMN_ORDERS_AMOUNT + " INTEGER, " + COLUMN_ORDERS_PRICE + " REAL)";
    private PreparedStatement createOrdersTable;

    public static final String CREATE_BASKET_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_BASKET + "(" + COLUMN_BASKET_ID + " INTEGER, " +
             COLUMN_BASKET_ITEMID + " INTEGER, " + COLUMN_BASKET_AMOUNT + " INTEGER)";
    private PreparedStatement createBasketTable;

    public static final String ADD_ITEM = "INSERT INTO " + TABLE_ITEMS + "(" + COLUMN_ITEMS_NAME + ", " + COLUMN_ITEMS_PRICE + ", " +
            COLUMN_ITEMS_STOCK + ") VALUES (?,?,?)";
    private PreparedStatement addItem;

    public static final String ADD_ORDER = "INSERT INTO " + TABLE_ORDERS + "(" + COLUMN_ORDERS_USERID + ", " + COLUMN_ORDERS_PRICE +
            ", " + COLUMN_ORDERS_AMOUNT + ") VALUES (?,?,?)";
    private PreparedStatement addOrder;

    public static final String ADD_BASKET = "INSERT INTO " + TABLE_BASKET + "(" + COLUMN_BASKET_ID + ", " + COLUMN_BASKET_ITEMID + ", " +
            COLUMN_BASKET_AMOUNT + ") VALUES (?,?,?)";
    private PreparedStatement addBasket;

    public static final String QUERY_USER = "SELECT " + COLUMN_USERS_FIRSTNAME + ", " + COLUMN_USERS_LASTNAME + ", " +
            COLUMN_USERS_PASSWORD + ", " + COLUMN_USERS_ISADMIN + ", " + COLUMN_USERS_ID + " FROM " + TABLE_USERS +
            " WHERE " + COLUMN_USERS_USERNAME + " = ? COLLATE NOCASE";
    private PreparedStatement queryUser;
    public static final String QUERY_ITEMS = "SELECT * FROM " + TABLE_ITEMS;
    private PreparedStatement queryItems;

    public static final String ADD_USER = "INSERT INTO " + TABLE_USERS + "(" + COLUMN_USERS_FIRSTNAME + ", " + COLUMN_USERS_LASTNAME + ", " +
            COLUMN_USERS_USERNAME + ", " + COLUMN_USERS_PASSWORD + ", " + COLUMN_USERS_ISADMIN + ") VALUES (?,?,?,?,0)" ;
    private PreparedStatement addUser;

    public static final String QUERY_ORDERS = "SELECT " + COLUMN_ORDERS_ID + ", " + COLUMN_ORDERS_AMOUNT + ", " + COLUMN_ORDERS_PRICE + " FROM " +
            TABLE_ORDERS + " WHERE " + COLUMN_ORDERS_USERID + " = ?";
    private PreparedStatement queryOrders;

    public static final String QUERY_BASKET_BY_ORDER_ID = "SELECT " + COLUMN_BASKET_ITEMID + ", " + COLUMN_BASKET_AMOUNT + " FROM " +
            TABLE_BASKET + " WHERE " + COLUMN_BASKET_ID + " = ?";
    private PreparedStatement queryBasketByOrderId;

    public static final String UPDATE_ITEM_STOCK = "UPDATE " + TABLE_ITEMS + " SET " + COLUMN_ITEMS_STOCK + " = ? WHERE " + COLUMN_ITEMS_ID + " = ?";
    private PreparedStatement updateItemStock;

//    public static final String SET_ITEM_STOCK = "UPDATE " + TABLE_ITEMS + " SET "
//    CREATE TABLE IF NOT EXISTS some_table (id INTEGER PRIMARY KEY AUTOINCREMENT, ...)

    private Connection conn;

    private static Datasource instance = new Datasource();
    private Datasource(){

    }

    public static Datasource getInstance() {
        return instance;
    }

    public boolean open() {
        try {
            conn = DriverManager.getConnection( CONNECTION_STRING );
            createUsersTable = conn.prepareStatement( CREATE_USERS_TABLE );
            createItemsTable = conn.prepareStatement( CREATE_ITEMS_TABLE );
            createOrdersTable = conn.prepareStatement( CREATE_ORDERS_TABLE );
            createBasketTable = conn.prepareStatement( CREATE_BASKET_TABLE );
            queryUser = conn.prepareStatement( QUERY_USER );
            queryItems = conn.prepareStatement( QUERY_ITEMS );
            addItem = conn.prepareStatement( ADD_ITEM );
            addOrder = conn.prepareStatement( ADD_ORDER );
            addBasket = conn.prepareStatement( ADD_BASKET );
            addUser = conn.prepareStatement( ADD_USER );
            queryOrders = conn.prepareStatement( QUERY_ORDERS );
            queryBasketByOrderId = conn.prepareStatement( QUERY_BASKET_BY_ORDER_ID );
            updateItemStock = conn.prepareStatement( UPDATE_ITEM_STOCK );
            createTables();






            return true;
        } catch (SQLException e) {
            System.out.println( "Could't connect to database " + e.getMessage() );
            e.printStackTrace();
            return false;
        }
    }

    public void close() {
        try {

            if (queryBasketByOrderId != null) ;
            {
                queryBasketByOrderId.close();
            }

            if (updateItemStock != null) ;
            {
                updateItemStock.close();
            }

            if (queryOrders != null) ;
            {
                queryOrders.close();
            }

            if (addOrder != null) ;
            {
                addOrder.close();
            }
            if (addBasket != null) ;
            {
                addBasket.close();
            }

            if (addUser != null) ;
            {
                addUser.close();
            }

            if (queryUser != null) ;
            {
                queryUser.close();
            }

            if (queryItems != null) ;
            {
                queryItems.close();
            }

            if (addItem != null) ;
            {
                addItem.close();
            }

            if (createOrdersTable != null) ;
            {
                createOrdersTable.close();
            }

            if (createItemsTable != null) ;
            {
                createItemsTable.close();
            }

            if (createUsersTable != null) ;
            {
                createUsersTable.close();
            }

            if (createBasketTable != null) ;
            {
                createBasketTable.close();
            }

            if (conn != null) ;
            {
                conn.close();
            }
        } catch (SQLException e) {
            System.out.println( "Couldn't close the connection " + e.getMessage() );
        }
    }

    public void createTables(){
        try {
            createUsersTable.executeUpdate();
            createItemsTable.executeUpdate();
            createOrdersTable.executeUpdate();
            createBasketTable.executeUpdate();

//            try(Scanner scanner = new Scanner( new BufferedReader( new FileReader( "items.txt" )))){
//                //scanner = new Scanner( new FileReader( "locations_big.txt" ) );
//                scanner.useDelimiter( "," );
//                while (scanner.hasNextLine()){
//                    String itemName = scanner.next();
//                    scanner.skip( scanner.delimiter() );
//                    double itemPrice = scanner.nextDouble();
//                    scanner.skip( scanner.delimiter() );
//                    int itemStock = Integer.parseInt( scanner.nextLine());
//
//                    addItem( itemName,itemPrice,itemStock );
//                }
//            } catch (IOException e ){
//                e.printStackTrace();
//            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public void addItem(String name, double price, int stock) throws SQLException{
        addItem.setString( 1, name);
        addItem.setDouble( 2, price );
        addItem.setInt( 3, stock );
        int affectedRows = addItem.executeUpdate();
        if (affectedRows != 1) {
            throw new SQLException( "Couldn't insert album" );
        }

    }

    public boolean queryUser(String username) throws SQLException{
        try {
            boolean queryRunned = false;
            queryUser.setString( 1, username );
            ResultSet results = queryUser.executeQuery();
            while(results.next()){
                User.getInstance().init( results.getString( 1 ), results.getString( 2 ), username,
                        results.getString( 3 ), results.getInt( 4 ), results.getInt( 5 ));
                queryRunned =true;
            }
            return queryRunned;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean addUser(String firstName, String lastName, String username, String password) throws SQLException{
        if (!queryUser(username)) {
            addUser.setString( 1, firstName );
            addUser.setString( 2, lastName );
            addUser.setString( 3, username );
            addUser.setString( 4, BCrypt.hashpw(password, BCrypt.gensalt( 10 )) );
            int affectedRows = addUser.executeUpdate();
            if (affectedRows != 1) {
                return false;
                //throw new SQLException( "Couldn't create account" );
            } else {
                return true;
            }
        } else {
            return false;
        }

    }

    public List<ItemProperty> queryItems(){
        try(ResultSet results = queryItems.executeQuery()) {

            List<ItemProperty> items = new ArrayList<>();
            while (results.next()) {
                ItemProperty itemProperty = new ItemProperty();
                itemProperty.setId( results.getInt( 1 ) );
                //System.out.println(results.getInt( 1 ));
                itemProperty.setName( results.getString( 2 ) );
                itemProperty.setPrice( results.getDouble( 3 ) );
                itemProperty.setStock( results.getInt( 4 ) );
                items.add( itemProperty );

            }
            System.out.println( "returning items list" );

            return items;
        } catch (SQLException e ) {
            e.printStackTrace();
            return null;
        }


    }

    public int addOrder(double price,int amount){
        try {
            addOrder.setInt( 1, User.getInstance().getId() );
            addOrder.setDouble( 2, price );
            addOrder.setInt( 3,amount );
            int affectedRows = addOrder.executeUpdate();
            if (affectedRows != 1) {
                throw new SQLException( "coulnd't add order" );
            }

            ResultSet generatedKeys = addOrder.getGeneratedKeys();
            if (generatedKeys.next()) {
                return generatedKeys.getInt( 1 );
            } else {
                throw new SQLException( "coulnd't get _id for order" );
            }
        } catch (SQLException  e){
            e.printStackTrace();
            return -1;
        }
    }

    public void addBasket(List<ItemProperty> items, int orderID) throws SQLException{
            for (ItemProperty item: items) {
                addBasket.setInt( 1, orderID );
                addBasket.setInt( 2,item.getId() );
                addBasket.setInt( 3,item.getAmountInBasket() );
                addBasket.executeUpdate();
                updateItemStock.setInt( 1, item.getStock() );
                updateItemStock.setInt( 2, item.getId() );
                updateItemStock.executeUpdate();

            }

    }

    public List<orderProperty> queryOrders() throws SQLException{

        queryOrders.setInt( 1, User.getInstance().getId() );
        ResultSet results = queryOrders.executeQuery();
        List<orderProperty> orders = new ArrayList<>(  );
        while (results.next()) {
            orderProperty op = new orderProperty();
            op.setOrderID( results.getInt( 1 ) );
            op.setTotalItems( results.getInt( 2 ) );
            op.setTotalPrice( results.getDouble( 3 ) );
            orders.add( op );

        }
        return orders;



    }

    public List<ItemProperty> queryBasketByOrderId(int orderId) throws SQLException{

        queryBasketByOrderId.setInt( 1, orderId);
        ResultSet results = queryBasketByOrderId.executeQuery();
        List<ItemProperty> orderedItems = new ArrayList<>(  );
        List<ItemProperty> allItems = new ArrayList<>( queryItems() );
        while (results.next()) {
            for (ItemProperty item: allItems) {
                if (item.getId()==results.getInt( 1 )){
                    item.setAmountInBasket( results.getInt( 2 ) );
                    orderedItems.add( item );
                }
            }
        }
        return orderedItems;
    }
}
