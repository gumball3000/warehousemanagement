package sample.Model;

public class Item {
    private String name;
    private double price;
    private int stock;
    private int id;

    public Item(String name, double price, int stock, int id) {
        this.name = name;
        this.price = price;
        this.stock = stock;
        this.id = id;
    }
}
