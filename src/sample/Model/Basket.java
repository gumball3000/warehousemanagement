package sample.Model;

import sample.ObjectProperty.ItemProperty;

import java.util.ArrayList;
import java.util.List;

public class Basket {
    private List<ItemProperty> items = new ArrayList<>(  );

    public void addItem(ItemProperty item){
        items.add( item );
    }

    public List<ItemProperty> getItems() {
        return  items;
    }

    public List<ItemProperty> getItemsList() {
        return items;
    }
}
