package sample.Model;


import java.util.List;

public class User {
    private String firstName;
    private String lastName;
    private String userName;
    private String password;
    private int isAdmin;
    private int id;

    private static User instance = new User();
    private User(){

    }

    public static User getInstance() {
        return instance;
    }

    public void init(String firstName, String lastName, String userName, String password, int isAdmin, int id){
        this.firstName = firstName;
        this.lastName = lastName;
        this.userName = userName;
        this. password = password;
        this.isAdmin = isAdmin;
        this.id = id;

    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getUserName() {
        return userName;
    }

    public String getPassword() {
        return password;
    }

    public int getIsAdmin() {
        return isAdmin;
    }

    public int getId() {
        return id;
    }
}
