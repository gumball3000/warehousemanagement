package sample;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import sample.BCrypt.BCrypt;
import sample.Model.Datasource;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        FXMLLoader loader = new FXMLLoader( getClass().getResource( "login.fxml" ) );
        Parent root = loader.load();
        //UserController controller = loader.getController();
        //controller.listItems();

        primaryStage.setTitle("Warehouse");
        primaryStage.setScene(new Scene(root, 310, 310));
        primaryStage.show();

//        System.out.println(BCrypt.hashpw("admin", BCrypt.gensalt( 10 )));
//        System.out.println(BCrypt.checkpw( "admin","$2a$10$A0r/9MXut8Mre4guXLc6s.c8Q.wK2YgJ1MjYRwbzeoeq4D3a4wxrm" ));
    }

    @Override
    public void init() throws Exception {
        super.init();
        if(!Datasource.getInstance().open()){
            System.out.println("Fatal error: Couldn't connect to database");
            Platform.exit();
        }
    }

    @Override
    public void stop() throws Exception {
        super.stop();
        Datasource.getInstance().close();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
