package sample;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

import java.util.Scanner;


public class addItemDialogController {
    @FXML
    private TextField amount;

    @FXML
    private Label label;

    public int processResults(){
        Scanner sc = new Scanner(amount.getText().trim());
        if(!sc.hasNextInt(10)) return -1;
        // we know it starts with a valid int, now make sure
        // there's nothing left!
        sc.nextInt(10);
        return Integer.parseInt( amount.getText().trim());
    }

    public void showLabel(){
        label.setVisible( true );
    }

    public void hideLabel(){
        label.setVisible( false );
    }

}
