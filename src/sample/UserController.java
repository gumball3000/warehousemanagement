package sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.*;
import sample.Model.Basket;
import sample.Model.Datasource;
import sample.ObjectProperty.ItemProperty;
import sample.ObjectProperty.orderProperty;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

public class UserController {
    @FXML
    private TableView itemTable;

    @FXML
    private Button button;

    @FXML
    private Button items;

    @FXML
    private Button myOrders;

    @FXML
    private Button myBasket;

    @FXML
    private TableView basketTable;

    @FXML
    private TableView orderTable;

    @FXML
    private Label priceLabel;

    private Basket basket = new Basket();
    private int page;


    @FXML
    public void listItems() throws SQLException {
        priceLabel.setVisible( false );
        button.setVisible( true );
        page = 0;
        itemTable.setVisible( true );
        basketTable.setVisible( false );
        orderTable.setVisible( false );
        button.setText( "Add to basket" );
        if (itemTable.getItems().isEmpty()) {
            Task<ObservableList<ItemProperty>> task = new Task<ObservableList<ItemProperty>>() {
                @Override
                protected ObservableList<ItemProperty> call() throws Exception {
                    return FXCollections.observableArrayList( Datasource.getInstance().queryItems() );
                }
            };
            itemTable.itemsProperty().bind( task.valueProperty() );

            new Thread( task ).start();
        }
    }

    @FXML
    public void handleButtonPress() throws SQLException {
        switch (page) {
            case 0:
                final ItemProperty item = (ItemProperty) itemTable.getSelectionModel().getSelectedItem();
                if (item != null) {
                    //basket.addItem( item );
                    showAddItemDialog();
                    System.out.println( "adding item" );
                }
                break;

            case 1:
                placeOrder();
                break;

            case 2:
                final orderProperty order = (orderProperty) orderTable.getSelectionModel().getSelectedItem();
                if (order !=null ) {
                    showOrderDetalis();
                }
                break;

        }
    }

    @FXML
    private void listBasket() {
        button.setVisible( true );
        page = 1;
        itemTable.setVisible( false );
        orderTable.setVisible( false );

        button.setText( "Place order" );
        Task<ObservableList<ItemProperty>> task = new Task<ObservableList<ItemProperty>>() {
            @Override
            protected ObservableList<ItemProperty> call() throws Exception {
                return FXCollections.observableArrayList( basket.getItems() );
            }
        };
        basketTable.itemsProperty().bind( task.valueProperty() );
        basketTable.setVisible( true );
        new Thread( task ).start();

        List<ItemProperty> items = new ArrayList<ItemProperty>();
        items.addAll( basket.getItems() );
        double totalBasketPrice = 0;
        int amount = 0;
        for (ItemProperty item : items) {
            totalBasketPrice += item.getPrice() * item.getAmountInBasket();
            amount += item.getAmountInBasket();
        }

        if (totalBasketPrice>0){
            priceLabel.setText( "Total price: " + totalBasketPrice );
            priceLabel.setVisible( true );


        }
    }

    @FXML
    public void listOrders() {
        priceLabel.setVisible( false );
        button.setVisible( true );
        page = 2;
        itemTable.setVisible( false );
        basketTable.setVisible( false );
        orderTable.setVisible( true );
        button.setText( "View Detalis" );

        Task<ObservableList<orderProperty>> task = new Task<ObservableList<orderProperty>>() {
            @Override
            protected ObservableList<orderProperty> call() throws Exception {
                return FXCollections.observableArrayList( Datasource.getInstance().queryOrders() );
            }
        };
        orderTable.itemsProperty().bind( task.valueProperty() );

        new Thread( task ).start();


    }

    @FXML
    public void showAddItemDialog() {
        Dialog<ButtonType> dialog = new Dialog<>();
        dialog.initOwner( itemTable.getScene().getWindow() );
        dialog.setTitle( "Input the number of items" );
        // dialog.setHeaderText( "Use this dialog to create a new Todo Item" );
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation( getClass().getResource( "addItemDialog.fxml" ) );
        try {
            dialog.getDialogPane().setContent( fxmlLoader.load() );
        } catch (IOException e) {
            System.out.println( "Couldn't load the dialog" );
            e.printStackTrace();
            return;
        }
        final ItemProperty item = (ItemProperty) itemTable.getSelectionModel().getSelectedItem();
        final addItemDialogController controller = fxmlLoader.getController();
        //dialog.getDialogPane().getButtonTypes().add( ButtonType.OK );
        dialog.getDialogPane().getButtonTypes().add( ButtonType.CANCEL );
        dialog.getDialogPane().getButtonTypes().add( ButtonType.OK );

        final Button okButton = (Button) dialog.getDialogPane().lookupButton( ButtonType.OK );
        okButton.addEventFilter( ActionEvent.ACTION, ae -> {
            if (controller.processResults() > item.getStock() || controller.processResults() <= 0) {
                ae.consume(); //not valid
                controller.showLabel();
            } else {
                controller.hideLabel();
            }
        } );

        Optional<ButtonType> result = dialog.showAndWait();
        if (result.isPresent() && result.get() == ButtonType.OK) {

//            if ( !basket.getItems().isEmpty()) {
//                Iterator<ItemProperty> iterator = basket.getItems().iterator();
//
//                while (iterator.hasNext()) {
//
//                    ItemProperty itemInBasket = iterator.next();
//
//                    if (itemInBasket.getAmountInBasket() == item.getAmountInBasket()) {
//
//                        item.setAmountInBasket( item.getAmountInBasket() + controller.processResults() );
//                    } else
//                        item.setAmountInBasket( controller.processResults() );
//                    basket.addItem( item );
//
//                }
//            }else {
//                item.setAmountInBasket( controller.processResults() );
//                basket.addItem( item );
//            }
            int foundItem = 0;
            if (!basket.getItems().isEmpty()) {
                for (ItemProperty itemInBasket : basket.getItems()) {
                    if (itemInBasket.getId() == item.getId()) {
                        itemInBasket.setAmountInBasket( itemInBasket.getAmountInBasket() + controller.processResults() );
                        item.setStock( item.getStock()-controller.processResults() );
                        foundItem = 1;
                    }

                }
            }
            if (foundItem == 0) {
                item.setAmountInBasket( controller.processResults() );
                basket.addItem( item );
                item.setStock( item.getStock()-controller.processResults()  );
            }

            System.out.println( "ok pressed" );
        } else {
            System.out.println( "cancel pressed" );
        }

    }

    public void placeOrder() throws SQLException {
        List<ItemProperty> items = new ArrayList<ItemProperty>();
        items.addAll( basketTable.getItems() );
        double totalBasketPrice = 0;
        int amount = 0;
        for (ItemProperty item : items) {
            totalBasketPrice += item.getPrice() * item.getAmountInBasket();
            amount += item.getAmountInBasket();
        }
        if (totalBasketPrice > 0) {
            int price = Datasource.getInstance().addOrder( totalBasketPrice, amount );
            Datasource.getInstance().addBasket( items, price );
            basket = new Basket();
            listBasket();
        }

    }

    public void showOrderDetalis(){
        final orderProperty order = (orderProperty) orderTable.getSelectionModel().getSelectedItem();
        Task<ObservableList<ItemProperty>> task = new Task<ObservableList<ItemProperty>>() {
            @Override
            protected ObservableList<ItemProperty> call() throws Exception {
                return FXCollections.observableArrayList( Datasource.getInstance().queryBasketByOrderId( order.getOrderID() ) );
            }
        };
        basketTable.itemsProperty().bind( task.valueProperty() );
        orderTable.setVisible( false );
        basketTable.setVisible( true );
        new Thread( task ).start();
        button.setVisible( false );
    }


}

