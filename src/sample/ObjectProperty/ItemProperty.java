package sample.ObjectProperty;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class ItemProperty {
    private SimpleStringProperty name;
    private SimpleDoubleProperty price;
    private SimpleIntegerProperty stock;
    private SimpleIntegerProperty id;
    private SimpleIntegerProperty amountInBasket;

    public ItemProperty() {
        this.name = new SimpleStringProperty(  );
        this.price = new SimpleDoubleProperty(  );
        this.stock = new SimpleIntegerProperty(  );
        this.id = new SimpleIntegerProperty(  );
        this.amountInBasket = new SimpleIntegerProperty(  );
    }

    public String getName() {
        return name.get();
    }

    public SimpleStringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set( name );
    }

    public double getPrice() {
        return price.get();
    }

    public SimpleDoubleProperty priceProperty() {
        return price;
    }

    public void setPrice(double price) {
        this.price.set( price );
    }

    public int getStock() {
        return stock.get();
    }

    public SimpleIntegerProperty stockProperty() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock.set( stock );
    }

    public int getId() {
        return id.get();
    }

    public SimpleIntegerProperty idProperty() {
        return id;
    }

    public void setId(int id) {
        this.id.set( id );
    }

    public int getAmountInBasket() {
        return amountInBasket.get();
    }

    public SimpleIntegerProperty amountInBasketProperty() {
        return amountInBasket;
    }

    public void setAmountInBasket(int amountInBasket) {
        this.amountInBasket.set( amountInBasket );
    }
}
