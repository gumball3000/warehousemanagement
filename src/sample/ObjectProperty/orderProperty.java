package sample.ObjectProperty;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;

public class orderProperty {
    private SimpleIntegerProperty orderID;
    private SimpleIntegerProperty totalItems;
    private SimpleDoubleProperty totalPrice;

    public orderProperty() {
        this.orderID = new SimpleIntegerProperty(  );
        this.totalItems = new SimpleIntegerProperty();
        this.totalPrice = new SimpleDoubleProperty();
    }

    public int getOrderID() {
        return orderID.get();
    }

    public SimpleIntegerProperty orderIDProperty() {
        return orderID;
    }

    public void setOrderID(int orderID) {
        this.orderID.set( orderID );
    }

    public int getTotalItems() {
        return totalItems.get();
    }

    public SimpleIntegerProperty totalItemsProperty() {
        return totalItems;
    }

    public void setTotalItems(int totalItems) {
        this.totalItems.set( totalItems );
    }

    public double getTotalPrice() {
        return totalPrice.get();
    }

    public SimpleDoubleProperty totalPriceProperty() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice.set( totalPrice );
    }
}
