package sample.Actions;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import sample.BCrypt.BCrypt;
import sample.Model.Datasource;
import sample.Model.User;
import sample.UserController;

import java.sql.SQLException;


public class LoginController {
    @FXML
    private TextField userField;

    @FXML
    private PasswordField passwordField;

    @FXML
    public void login(ActionEvent event) throws Exception {
        String username = userField.getText().trim();
        String password = passwordField.getText().trim();
        if (Datasource.getInstance().queryUser( username )){
            if(User.getInstance().getPassword() != null) {
                if (BCrypt.checkpw( password, User.getInstance().getPassword() )) {
                    System.out.println( "Doing ok" );
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("/sample/User.fxml"));
                    Parent userPanel = loader.load();
                    Stage stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
                    Scene signupScene = new Scene (userPanel, 655, 366);
                    stage.setScene(signupScene);
                    stage.show();
                    UserController controller = loader.getController();
                    controller.listItems();
                    stage.setResizable(false);
                }
            }
        }
    }

    @FXML
    public void signup(ActionEvent event) throws Exception{
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/sample/Signup.fxml"));
        Parent signup = loader.load();

        //use one of components on your scene to get a reference to your scene object.

        Stage stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
        Scene signupScene = new Scene (signup, 330, 330);
        stage.setScene(signupScene);
        stage.show();
        stage.setResizable(false);

    }
}
