package sample.Actions;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import sample.Model.Datasource;

import java.sql.SQLException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class SignupController {

    @FXML
    private TextField firstName;

    @FXML
    private TextField lastName;

    @FXML
    private TextField userName;

    @FXML
    private PasswordField password;

    @FXML
    private PasswordField password2;

    @FXML
    private Label label;

    @FXML
    public void signup(ActionEvent event) throws Exception {
        if (password.getText().equals( password2.getText() )) {
            if (password.getLength() >= 5) {
                if (!hasSpecialCharacter( userName.getText() )) {
                    if (!hasSpecialCharacter( firstName.getText()) && !hasSpecialCharacter( lastName.getText() )) {
                        if (Datasource.getInstance().addUser( firstName.getText().trim(), lastName.getText().trim(), userName.getText().trim(), password.getText() )) {
                            System.out.println( "User added" );

                            FXMLLoader loader = new FXMLLoader( getClass().getResource( "/sample/login.fxml" ) );
                            Parent login = loader.load();
                            Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                            Scene signupScene = new Scene( login, 310, 310 );
                            stage.setScene( signupScene );
                            stage.show();
                            stage.setResizable( false );

                        } else {
                            label.setText( "Username taken" );
                            label.setVisible( true );
                        }
                    } else {
                        label.setText( "Invalid name" );
                        label.setVisible( true );
                    }
                } else {
                    label.setText( "Invalid Username" );
                    label.setVisible( true );
                }
            } else {
                label.setText( "Password must be at least 5 charcacters long" );
                label.setVisible( true );
            }
        } else {
            label.setText( "Passwords do not mach" );
            label.setVisible( true );
        }
    }

    public boolean hasSpecialCharacter(String text){
        Pattern sPattern = Pattern.compile( "[a-zA-Z0-9]*" );
        Matcher sMatcher = sPattern.matcher( text );

        if(!sMatcher.matches()){
            return true;
        } else {
            return false;
        }
    }

}
